// SPDX-License-Identifier: MIT
#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESPAsyncTCP.h>
#include <ESP8266WebServer.h>
#include <fauxmoESP.h>
#define WIFI_SSID ""
#define WIFI_PASS ""
#define SERIAL_BAUDRATE 115200
fauxmoESP fauxmo;

int relay1 = 14; //D5
int relay2 = 12; //D6
int relay3 = 13; //D7
int relay4 = 5;  //D1

const char* zoneOne = "Front Sprinklers";
const char* zoneTwo = "Back Sprinklers";
const char* zoneThree = "Patio Sprinklers";
const char* zoneFour = "Wall Sprinklers";
String ipAddress ="";
int webserverPort = 81;
int deviceServerPort = 80;
ESP8266WebServer server(webserverPort);
 
// -----------------------------------------------------------------------------
// Wifi
// -----------------------------------------------------------------------------
void wifiSetup() {
  // Set WIFI module to STA mode
  WiFi.mode(WIFI_STA);
  // Connect
  Serial.printf("[WIFI] Connecting to %s ", WIFI_SSID);
  WiFi.begin(WIFI_SSID, WIFI_PASS);
  // Wait
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    delay(100);
  }
  Serial.println();
  // Connected!
  Serial.printf("[WIFI] STATION Mode, SSID: %s, IP address: %s\n",
  WiFi.SSID().c_str(), WiFi.localIP().toString().c_str());
  ipAddress = WiFi.localIP().toString().c_str();
  pinMode(relay1,OUTPUT);
  pinMode(relay2,OUTPUT);
  pinMode(relay3,OUTPUT);
  pinMode(relay4,OUTPUT);
}

void serverSetup(){
  server.on("/", homePage);
  server.on("/relay", handleCall);
  server.onNotFound(notFound);
  
  server.begin();
  Serial.println("HTTP server started");
}

void setup() {
 // Init serial port and clean garbage
 Serial.begin(SERIAL_BAUDRATE);
 Serial.println("FauxMo demo sketch");
 Serial.println("After connection, ask Alexa/Echo to 'turn <devicename> on' or 'off'");
 // Wifi
 wifiSetup();
 //webserver
 
 // Fauxmo
 fauxmo.addDevice(zoneOne);
 fauxmo.addDevice(zoneTwo);
 fauxmo.addDevice(zoneThree);
 fauxmo.addDevice(zoneFour);
 // Gen3 Devices or above
 fauxmo.setPort(deviceServerPort);
 // Allow the FauxMo to be discovered
 fauxmo.enable(true);
 
 fauxmo.onSetState([](unsigned char device_id, const char * device_name, bool state, unsigned char value) {
   
   
   
   if (strcmp(device_name,zoneOne)==0) {
    
      trigerRelay(state,relay1,zoneOne);
   
   } else if (strcmp(device_name,zoneTwo)==0) {
    
      trigerRelay(state,relay2,zoneTwo);
      
   } else if (strcmp(device_name,zoneThree)==0) {
      
      trigerRelay(state,relay3,zoneThree);
      
   } else if (strcmp(device_name,zoneFour)==0) {
    
      trigerRelay(state,relay4,zoneFour);
      
   } 
  
 });
  digitalWrite(relay1,  HIGH );
  digitalWrite(relay2,  HIGH );
  digitalWrite(relay3,  HIGH );
  digitalWrite(relay4,  HIGH );

  serverSetup();
}
void loop() {
  
 fauxmo.handle();
 server.handleClient();
 
}
void handleCall(){

  String relayNumberValue = "";
  String stateValue = "";
  
  if( server.hasArg("relayNumber") && server.arg("state") ) {

    relayNumberValue = server.arg("relayNumber");
    stateValue = server.arg("state");
    
    Serial.print("Device: ");
    Serial.print(relayNumberValue);
    Serial.print(" state: ");
    Serial.print(stateValue);
    
    if (relayNumberValue == "zoneOne"){
      if (stateValue == "true"){
        trigerRelay(true,relay1,zoneOne);
      } else{
        trigerRelay(false,relay1,zoneOne);
      }
    } else if (relayNumberValue == "zoneTwo"){
      if (stateValue == "true"){
        trigerRelay(true,relay2,zoneTwo);
      } else{
        trigerRelay(false,relay2,zoneTwo);
      }
    } else if (relayNumberValue == "zoneThree"){
      if (stateValue == "true"){
        trigerRelay(true,relay3,zoneThree);
      } else{
        trigerRelay(false,relay3,zoneThree);
      }
    } else if (relayNumberValue == "zoneFour"){
      if (stateValue == "true"){
        trigerRelay(true,relay4,zoneFour);
      } else{
        trigerRelay(false,relay4,zoneFour);
      }
    }
  }
}
void homePage(){
  String html = "<html lang=\"en\">";
    html += "<html lang=\"en\">";
    html += "  <head>";
    html += "   <meta charset=\"UTF-8\">";
    html += "   <meta name=\"apple-mobile-web-app-title\" content=\"CodePen\">";
    html += "   <title>Relay Switch Home Page</title>";
    html += "   <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">";
    html += "   <style>";
    html += "     html {";
    html += "       box-sizing: border-box;";
    html += "     }";
    html += "     *,";
    html += "     *:before,";
    html += "     *:after {";
    html += "       box-sizing: inherit;";
    html += "     }";
    html += "     body {";
    html += "       background-image: linear-gradient(48deg, #000000 0%, #072336 70%);";
    html += "       color: #121943;";
    html += "       display: grid;";
    html += "       flex-direction: column;";
    html += "       font-family: \"Lato\", sans-serif;";
    html += "       font-size: 1em;";
    html += "       height: 100vh;";
    html += "       place-items: center;";
    html += "       line-height: 1.2;";
    html += "     }";
    html += "     .heading {";
    html += "       font-size: 1.5em;";
    html += "       margin-bottom: 12px;";
    html += "     }";
    html += "     .card {";
    html += "       background: #fff;";
    html += "       background-image: linear-gradient(48deg, #fcfdff 0%, #cad2d5 50%);";
    html += "       border-top-right-radius: 16px;";
    html += "       border-bottom-left-radius: 16px;";
    html += "       box-shadow: -20px 20px 35px 1px rgba(10, 49, 86, 0.18);";
    html += "       display: flex;";
    html += "       flex-direction: column;";
    html += "       padding: 32px;";
    html += "       margin: 40px;";
    html += "       max-width: 400px;";
    html += "       width: 100%;";
    html += "     }";
    html += "     .content-wrapper {";
    html += "       font-size: 1.1em;";
    html += "       margin-bottom: 44px;";
    html += "     }";
    html += "     .content-wrapper:last-child {";
    html += "       margin-bottom: 0;";
    html += "     }";
    html += "     .button {";
    html += "       align-items: center;";
    html += "       background: #e5efe9;";
    html += "       border: 1px solid #5a72b5;";
    html += "       border-radius: 4px;";
    html += "       color: #121943;";
    html += "       cursor: pointer;";
    html += "       display: flex;";
    html += "       font-size: 1em;";
    html += "       font-weight: 700;";
    html += "       height: 40px;";
    html += "       justify-content: center;";
    html += "       width: 150px;";
    html += "     }";
    html += "     .button:focus {";
    html += "       border: 2px solid transparent;";
    html += "       box-shadow: 0px 0px 0px 2px #121943;";
    html += "       outline: solid 4px transparent;";
    html += "     }";
    html += "     .link {";
    html += "       color: #121943;";
    html += "     }";
    html += "     .link:focus {";
    html += "       box-shadow: 0px 0px 0px 2px #121943;";
    html += "     }";
    html += "     .input-wrapper {";
    html += "       display: flex;";
    html += "       flex-direction: column;";
    html += "     }";
    html += "     .input-wrapper .label {";
    html += "       align-items: baseline;";
    html += "       display: flex;";
    html += "       font-weight: 700;";
    html += "       justify-content: space-between;";
    html += "       margin-bottom: 8px;";
    html += "     }";
    html += "     .input-wrapper .optional {";
    html += "       color: #5a72b5;";
    html += "       font-size: 0.9em;";
    html += "     }";
    html += "     .input-wrapper .input {";
    html += "       border: 1px solid #5a72b5;";
    html += "       border-radius: 4px;";
    html += "       height: 40px;";
    html += "       padding: 8px;";
    html += "     }";
    html += "     code {";
    html += "       background: #e5efe9;";
    html += "       border: 1px solid #5a72b5;";
    html += "       border-radius: 4px;";
    html += "       padding: 2px 4px;";
    html += "     }";
    html += "     /* ";
    html += "     The following vars come from my theme. ";
    html += "     You'll need to replace with your own color values. ";
    html += "     - \"$light\"";
    html += "     - \"$mid\"";
    html += "     - \"$dark\"";
    html += "     */";
    html += "     .toggle {";
    html += "       align-items: center;";
    html += "       border-radius: 100px;";
    html += "       display: flex;";
    html += "       font-weight: 700;";
    html += "       margin-bottom: 16px;";
    html += "     }";
    html += "     .toggle:last-of-type {";
    html += "       margin: 0;";
    html += "     }";
    html += "     .toggle__input {";
    html += "       clip: rect(0 0 0 0);";
    html += "       clip-path: inset(50%);";
    html += "       height: 1px;";
    html += "       overflow: hidden;";
    html += "       position: absolute;";
    html += "       white-space: nowrap;";
    html += "       width: 1px;";
    html += "     }";
    html += "     .toggle__input:not([disabled]):active + .toggle-track, .toggle__input:not([disabled]):focus + .toggle-track {";
    html += "       border: 1px solid transparent;";
    html += "       box-shadow: 0px 0px 0px 2px #121943;";
    html += "     }";
    html += "     .toggle__input:disabled + .toggle-track {";
    html += "       cursor: not-allowed;";
    html += "       opacity: 0.7;";
    html += "     }";
    html += "     .toggle-track {";
    html += "       background: #e5efe9;";
    html += "       border: 1px solid #5a72b5;";
    html += "       border-radius: 100px;";
    html += "       cursor: pointer;";
    html += "       display: flex;";
    html += "       height: 30px;";
    html += "       margin-right: 12px;";
    html += "       position: relative;";
    html += "       width: 60px;";
    html += "     }";
    html += "     .toggle-indicator {";
    html += "       align-items: center;";
    html += "       background: #121943;";
    html += "       border-radius: 24px;";
    html += "       bottom: 2px;";
    html += "       display: flex;";
    html += "       height: 24px;";
    html += "       justify-content: center;";
    html += "       left: 2px;";
    html += "       outline: solid 2px transparent;";
    html += "       position: absolute;";
    html += "       transition: 0.25s;";
    html += "       width: 24px;";
    html += "     }";
    html += "     .checkMark {";
    html += "       fill: #fff;";
    html += "       height: 20px;";
    html += "       width: 20px;";
    html += "       opacity: 0;";
    html += "       transition: opacity 0.25s ease-in-out;";
    html += "     }";
    html += "     .toggle__input:checked + .toggle-track .toggle-indicator {";
    html += "       background: #121943;";
    html += "       transform: translateX(30px);";
    html += "     }";
    html += "     .toggle__input:checked + .toggle-track .toggle-indicator .checkMark {";
    html += "       opacity: 1;";
    html += "       transition: opacity 0.25s ease-in-out;";
    html += "     }";
    html += "     @media screen and (-ms-high-contrast: active) {";
    html += "       .toggle-track {";
    html += "       border-radius: 0;";
    html += "       }";
    html += "     }";
    html += "   </style>";
    html += "   <script>";
    html += "    function toggleRelay(relayElement){";
    html += "         var relayUrl = \"http://"+ipAddress+":"+webserverPort+"/relay?relayNumber=\"+relayElement.id+\"&state=\"+relayElement.checked; ";
    html += "     fetch(relayUrl)";
    html += "       .then(response => response.json()).then(data =>  function(data){ $(\"#response\").html(JSON.stringify(data)); });";
    html += "    }";
    html += "   </script>";
    html += " </head>";
    html += " <body translate=\"no\" data-new-gr-c-s-check-loaded=\"14.1052.0\" data-gr-ext-installed=\"\">";
    html += "   <div class=\"card\">";
    html += "   <div class=\"content-wrapper\">";
    html += "     <h2 class=\"heading\">Zone Selection</h2>";
    html += "     <p id='response' ></p>";
    html += "   </div>";
    html += "   <div class=\"demo\">";
    html += "     <!-- begin toggle markup   -->";
    html += "     <label class=\"toggle\" for=\"zoneOne\">";
    html += "       <input type=\"checkbox\" class=\"toggle__input\" id=\"zoneOne\" onclick=\"toggleRelay(this);\">";
    html += "       <span class=\"toggle-track\">";
    html += "         <span class=\"toggle-indicator\">";
    html += "           <!--  This check mark is optional  -->";
    html += "           <span class=\"checkMark\">";
    html += "             <svg viewBox=\"0 0 24 24\" id=\"ghq-svg-check\" role=\"presentation\" aria-hidden=\"true\">";
    html += "               <path d=\"M9.86 18a1 1 0 01-.73-.32l-4.86-5.17a1.001 1.001 0 011.46-1.37l4.12 4.39 8.41-9.2a1 1 0 111.48 1.34l-9.14 10a1 1 0 01-.73.33h-.01z\"></path>";
    html += "             </svg>";
    html += "           </span>";
    html += "         </span>";
    html += "       </span>";
    html +=        zoneOne;
    html += "     </label>";
    html += "     <!-- end toggle markup   -->";
    html += "     <!-- begin toggle markup   -->";
    html += "     <label class=\"toggle\" for=\"zoneTwo\">";
    html += "       <input type=\"checkbox\" class=\"toggle__input\" id=\"zoneTwo\" onclick=\"toggleRelay(this);\">";
    html += "       <span class=\"toggle-track\">";
    html += "         <span class=\"toggle-indicator\">";
    html += "           <!--  This check mark is optional  -->";
    html += "           <span class=\"checkMark\">";
    html += "             <svg viewBox=\"0 0 24 24\" id=\"ghq-svg-check\" role=\"presentation\" aria-hidden=\"true\">";
    html += "               <path d=\"M9.86 18a1 1 0 01-.73-.32l-4.86-5.17a1.001 1.001 0 011.46-1.37l4.12 4.39 8.41-9.2a1 1 0 111.48 1.34l-9.14 10a1 1 0 01-.73.33h-.01z\"></path>";
    html += "             </svg>";
    html += "           </span>";
    html += "         </span>";
    html += "       </span>";
    html +=         zoneTwo;
    html += "     </label>";
    html += "     <!-- end toggle markup   -->";
    html += "     <!-- begin toggle markup   -->";
    html += "     <label class=\"toggle\" for=\"zoneThree\">";
    html += "       <input type=\"checkbox\" class=\"toggle__input\" id=\"zoneThree\" onclick=\"toggleRelay(this);\">";
    html += "       <span class=\"toggle-track\">";
    html += "         <span class=\"toggle-indicator\">";
    html += "           <!--  This check mark is optional  -->";
    html += "           <span class=\"checkMark\">";
    html += "             <svg viewBox=\"0 0 24 24\" id=\"ghq-svg-check\" role=\"presentation\" aria-hidden=\"true\">";
    html += "               <path d=\"M9.86 18a1 1 0 01-.73-.32l-4.86-5.17a1.001 1.001 0 011.46-1.37l4.12 4.39 8.41-9.2a1 1 0 111.48 1.34l-9.14 10a1 1 0 01-.73.33h-.01z\"></path>";
    html += "             </svg>";
    html += "           </span>";
    html += "         </span>";
    html += "       </span>";
    html +=        zoneThree;
    html += "     </label>";
    html += "     <!-- end toggle markup   -->";
    html += "     <!-- begin toggle markup   -->";
    html += "     <label class=\"toggle\" for=\"zoneFour\">";
    html += "       <input type=\"checkbox\" class=\"toggle__input\" id=\"zoneFour\" onclick=\"toggleRelay(this);\">";
    html += "       <span class=\"toggle-track\">";
    html += "         <span class=\"toggle-indicator\">";
    html += "           <!--  This check mark is optional  -->";
    html += "           <span class=\"checkMark\">";
    html += "             <svg viewBox=\"0 0 24 24\" id=\"ghq-svg-check\" role=\"presentation\" aria-hidden=\"true\">";
    html += "               <path d=\"M9.86 18a1 1 0 01-.73-.32l-4.86-5.17a1.001 1.001 0 011.46-1.37l4.12 4.39 8.41-9.2a1 1 0 111.48 1.34l-9.14 10a1 1 0 01-.73.33h-.01z\"></path>";
    html += "             </svg>";
    html += "           </span>";
    html += "         </span>";
    html += "       </span>";
    html +=        zoneFour;
    html += "     </label>";
    html += "     <!-- end toggle markup   -->";
    html += "   </div>";
    html += " </div>";
    html += " </body>";
    html += "</html>";
    server.send(200, "text/html", html); 
}
void notFound(){
  server.send(404, "text/plain", "Not found");
}
void trigerRelay(boolean turnOn,int relayPin, String niceName) {
  if (turnOn){
   
   digitalWrite(relayPin,  LOW );
   Serial.println(" - ON!");
   server.send(200, "text/html", "Relay "+niceName+ " is ON"); 
   delay(1000);
      
  } else {
      
   digitalWrite(relayPin,  HIGH );
   Serial.println(" - OFF!");
   server.send(200, "text/html", "Relay "+niceName+ " is OFF"); 
   delay(1000);
     
  }
}
